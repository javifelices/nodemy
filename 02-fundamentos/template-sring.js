let nombre = 'Iron Man';
let real = 'Tony Stark';


console.log(`${nombre} - ${real}`);

let nombreCompleto = nombre + ' ' + real;
let nombreTemplate = `${nombre} ${real}`;

console.log(nombreCompleto === nombreTemplate);

function getNombre () {
    return `${nombre} es ${real}`;
}

console.log(`El nombre de ${getNombre()}`);